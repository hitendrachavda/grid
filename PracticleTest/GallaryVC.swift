//
//  GallaryVC.swift
//  PracticleTest
//
//  Created by Hitendra on 15/10/20.
//  Copyright © 2020 Hitendra. All rights reserved.
//

import UIKit

class PhotoCell : UICollectionViewCell {
    @IBOutlet weak var photo : UIImageView?
}

class GallaryVC: UIViewController {

    @IBOutlet weak var scrollView : UIScrollView?
    @IBOutlet weak var collectionView : UICollectionView?
    @IBOutlet weak var collectionViewHeight : NSLayoutConstraint?
    var arrPhotos = [UIImage]()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        let image = UIImage(named: "nature")
        for _ in 1...25 {
            arrPhotos.append(image!)
        }
        self.collectionView?.reloadData()
    }
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        self.collectionView?.reloadData()
        collectionView!.layoutIfNeeded()
        let contentSize = collectionView!.contentSize
        collectionViewHeight?.constant = contentSize.height
        scrollView!.contentSize = contentSize
    }
    
    @IBAction func back(_ sender : UIButton){
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func gallary2(_ sender : UIButton){
        let storyBoard = UIStoryboard(name: "Main", bundle: nil)
        let gallary2 = storyBoard.instantiateViewController(withIdentifier: "Gallary2") as! Gallary2
        self.navigationController?.pushViewController(gallary2, animated: true)
    }
}
extension GallaryVC : UICollectionViewDelegate,UICollectionViewDataSource,UICollectionViewDelegateFlowLayout{
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        self.arrPhotos.count
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize{
        
        let width = collectionView.frame.size.width - 40
        return (CGSize(width: width/3, height: width/3))
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "PhotoCell", for: indexPath as IndexPath) as! PhotoCell
        let width = collectionView.frame.size.width - 40
        let size =  (CGSize(width: width/3, height: width/3))
        let image = self.arrPhotos[indexPath.row]
        cell.photo!.image = image
        cell.photo!.frame = CGRect(x: 0, y: 0, width: size.width, height: size.height)
        return cell
    }
}
    
