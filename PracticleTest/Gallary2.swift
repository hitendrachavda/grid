//
//  Gallary2.swift
//  PracticleTest
//
//  Created by Hitendra on 16/10/20.
//  Copyright © 2020 Hitendra. All rights reserved.
//

import UIKit

class Gallary2: UIViewController {

    var arrPhotos = [UIImage]()
    var scrollView = UIScrollView()
    var collectionView : UICollectionView? = nil
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        let image = UIImage(named: "nature")
        for _ in 1...25 {
            arrPhotos.append(image!)
        }
        let safeArea = self.getTopBottomInset()
        let scrollViewY = 65 + 10 + safeArea.top
        let scrollViewHeight = self.view.frame.size.height - scrollViewY - safeArea.bottom
        scrollView = UIScrollView(frame: CGRect(x: 0, y: scrollViewY, width: self.view.frame.size.width, height: scrollViewHeight))
        let flowlayout = UICollectionViewFlowLayout()
        collectionView = UICollectionView(frame: scrollView.bounds, collectionViewLayout: flowlayout)
        collectionView!.backgroundColor = .white
        collectionView!.register(UICollectionViewCell.self, forCellWithReuseIdentifier: "cell")
        collectionView!.delegate = self
        collectionView!.dataSource = self
        collectionView!.contentInset = UIEdgeInsets(top: 0, left: 10, bottom: 0, right: 10)
        collectionView!.isScrollEnabled = false
        scrollView.addSubview(collectionView!)
        collectionView!.reloadData()
        self.view.addSubview(scrollView)
        collectionView!.layoutIfNeeded()
        let contentSize = collectionView!.contentSize
        collectionView!.frame = CGRect(x: 0, y: 0, width: self.view.frame.size.width, height: contentSize.height)
        scrollView.contentSize = contentSize
    }
    @IBAction func back(_ sender : UIButton){
        self.navigationController?.popViewController(animated: true)
    }
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        let safeArea = self.getTopBottomInset()
        let scrollViewY = 65 + 10 + safeArea.top
        let scrollViewHeight = self.view.frame.size.height - scrollViewY - safeArea.bottom
        scrollView.frame = CGRect(x: 0, y: scrollViewY, width: self.view.frame.size.width, height: scrollViewHeight)
        collectionView!.reloadData()

    }
    
    func getTopBottomInset() -> (top : CGFloat, bottom : CGFloat){
    
        var topArea : CGFloat = 0.0
        var bottomArea : CGFloat = 0.0
        if #available(iOS 11.0, *){
            let safeAreaInsets = UIApplication.shared.windows.first{$0.isKeyWindow}?.safeAreaInsets
            topArea = safeAreaInsets!.top
            bottomArea = safeAreaInsets!.bottom
        }
        return (topArea,bottomArea)
    }
    
}
extension Gallary2 : UICollectionViewDelegate,UICollectionViewDataSource,UICollectionViewDelegateFlowLayout{
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        self.arrPhotos.count
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize{
        let width = collectionView.frame.size.width - 40
        return (CGSize(width: width/3, height: width/3))
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "cell", for: indexPath as IndexPath)
        let width = collectionView.frame.size.width - 40
        let size =  (CGSize(width: width/3, height: width/3))
        let image = self.arrPhotos[indexPath.row]
        let photo = UIImageView(frame: CGRect(x: 0, y: 0, width: size.width, height: size.height))
        photo.image = image
        cell.contentView.addSubview(photo)
        photo.contentMode = .scaleAspectFit
        return cell
    }
   
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
        return 5
    }
}
    
