//
//  ViewController2.swift
//  PracticleTest
//
//  Created by Mac on 15/09/22.
//  Copyright © 2022 Hitendra. All rights reserved.
//

import UIKit

class PhotoCell1 : UICollectionViewCell {
    @IBOutlet weak var photo : UIImageView?
}
class ViewController2: UIViewController {

    @IBOutlet weak var collectionView : UICollectionView?
    var arrPhotos = [UIImage]()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        let image = UIImage(named: "nature")
        for _ in 1...25 {
            arrPhotos.append(image!)
        }
        self.collectionView?.reloadData()
    }
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        self.collectionView?.reloadData()
        collectionView!.layoutIfNeeded()
//        let contentSize = collectionView!.contentSize
//        collectionViewHeight?.constant = contentSize.height
//        scrollView!.contentSize = contentSize
    }
}

extension ViewController2 : UICollectionViewDataSource,UICollectionViewDelegate, UICollectionViewDelegateFlowLayout{
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        self.arrPhotos.count
    }
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "PhotoCell1", for: indexPath as IndexPath) as! PhotoCell1
        let width = collectionView.frame.size.width - 40
        let size =  (CGSize(width: width/3, height: width/3))
        let image = self.arrPhotos[indexPath.row]
        cell.photo!.image = image
        cell.photo!.frame = CGRect(x: 0, y: 0, width: size.width, height: size.height)
        return cell
    }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        let width = collectionView.frame.width - 40
        return (CGSize(width: width/3, height: width/3))
    }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
        return 5
    }
}
