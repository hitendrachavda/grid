//
//  ViewController.swift
//  PracticleTest
//
//  Created by Hitendra on 15/10/20.
//  Copyright © 2020 Hitendra. All rights reserved.
//

import UIKit

class TimeSheetCell: UITableViewCell {
    
    @IBOutlet weak var lblDate : UILabel?
    @IBOutlet weak var lblUserName : UILabel?
    @IBOutlet weak var lblInout : UILabel?
    @IBOutlet weak var lblTime : UILabel?
    
    @IBOutlet weak var stackView : UIStackView?
    @IBOutlet weak var stackHeigt : NSLayoutConstraint?
    
    override func layoutSubviews() {
        super.layoutSubviews()
        contentView.frame = contentView.frame.inset(by: UIEdgeInsets(top: 0, left: 0, bottom: 10, right: 0))
        contentView.layer.cornerRadius = 5
    }
    
    func loadTimeData(timeSheet : [TimeSheet]){
        let subViewArray = stackView!.arrangedSubviews
        for subView in subViewArray {
            stackView?.removeArrangedSubview(subView)
        }
        stackHeigt?.constant = 0
        let stackHeigh:CGFloat = 40
        var times:Int = 1
        for timeS in timeSheet{
            let lable = UILabel(frame: CGRect(x: 0, y: 0, width: self.frame.size.width, height: 40))
            lable.backgroundColor = .white
            lable.text = timeS.inouttime
            lable.numberOfLines = 1
            lable.textColor = .gray
            stackView?.addArrangedSubview(lable)
            stackHeigt?.constant = stackHeigh * CGFloat(times)
            stackView?.backgroundColor = .black
            times = times + 1
        }
    }
}

struct TimeSheet {
    var inouttime : String?
    var time : String?
}

struct Users {
    var name : String?
    var time : String?
    var timesheet = [TimeSheet]()
}

struct DateWise {
    var date : String?
    var time : String?
    var users = [Users]()
}

class ViewController: UIViewController {

    @IBOutlet weak var tableView : UITableView?
    var arrayTimeSheet = [DateWise]()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        let time1 = TimeSheet(inouttime: "In 10:00 AM - Out 01:30 PM", time: "3h 30m")
        let time2 = TimeSheet(inouttime: "In 10:00 AM - Out 01:30 PM", time: "3h 30m")
        let arrayTime = [time1,time2]
        
        let user1 = Users(name: "Banjamin Carford", time: "10m 30m", timesheet: arrayTime)
        let user2 = Users(name: "Pamela Pelletier", time: "10m 30m", timesheet: arrayTime)
        let arrayUsers = [user1,user2]
        
        let datewise1 = DateWise(date: "26/6/2020", time: "10m 30m", users: arrayUsers)
        let datewise2 = DateWise(date: "27/6/2020", time: "05m 30m", users: arrayUsers)
        let arryaDateWise = [datewise1,datewise2]
        
        self.arrayTimeSheet.append(contentsOf: arryaDateWise)
    }
    
     @IBAction func openGallary(_ sender : UIButton){
           let storyBoard = UIStoryboard(name: "Main", bundle: nil)
           let gallaryVC = storyBoard.instantiateViewController(withIdentifier: "GallaryVC") as! GallaryVC
        self.navigationController?.pushViewController(gallaryVC, animated: true)
     }
}

extension ViewController: UITableViewDelegate, UITableViewDataSource{
    
    func numberOfSections(in tableView: UITableView) -> Int {
        self.arrayTimeSheet.count
    }
    func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
        return 1.0
    }
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 40
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat{
        let height:CGFloat = 105
        let userTimeSheet = self.arrayTimeSheet[indexPath.section]
        let users = userTimeSheet.users
        let user = users[indexPath.row]
        let heightNew = user.timesheet.count-1
        return height + CGFloat(heightNew * 40)
    }
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let view = UIView()
        let vc = UIView(frame: CGRect(x: 20, y: 0, width: tableView.frame.size.width-40, height: 40))
         let userTimeSheet = self.arrayTimeSheet[section]
        let lable = UILabel()
        lable.heightAnchor.constraint(equalToConstant: 40).isActive = true
        lable.widthAnchor.constraint(equalToConstant: tableView.frame.size.width/2).isActive = true
        lable.text = userTimeSheet.date
        lable.textColor = .gray
        let lable2 = UILabel()
        lable2.heightAnchor.constraint(equalToConstant: 40).isActive = true
        lable2.widthAnchor.constraint(equalToConstant: tableView.frame.size.width/2).isActive = true
        lable2.text = userTimeSheet.time
        lable2.textColor = .blue
        lable2.textAlignment = .right
        
        let stackView  = UIStackView()
        stackView.axis = .horizontal
        stackView.distribution = .equalSpacing
        stackView.alignment = .center
        stackView.spacing = 0
            
        stackView.addArrangedSubview(lable)
        stackView.addArrangedSubview(lable2)
        stackView.translatesAutoresizingMaskIntoConstraints = false;
        vc.addSubview(stackView)
        stackView.centerXAnchor.constraint(equalTo: vc.centerXAnchor).isActive = true
        stackView.centerYAnchor.constraint(equalTo: vc.centerYAnchor).isActive = true
        view.addSubview(vc)
        return view
    }

    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        let userTimeSheet = self.arrayTimeSheet[section]
        let users = userTimeSheet.users
        return users.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let CellIdentifier = "TimeSheetCell"
        let cell = tableView.dequeueReusableCell(withIdentifier: CellIdentifier) as? TimeSheetCell
        let userTimeSheet = self.arrayTimeSheet[indexPath.section]
        let users = userTimeSheet.users
        let user = users[indexPath.row]
        cell?.lblInout?.text = user.name
        cell?.lblTime?.text = user.time
        cell?.loadTimeData(timeSheet: user.timesheet)
        cell?.selectionStyle = UITableViewCell.SelectionStyle.none
        return cell!
    }
}
